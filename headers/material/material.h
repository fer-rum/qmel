#pragma once

#include <Qt3DRender>

namespace qmel {

class Material
{
    public:

        /**
         * @brief The State enum is used in conjunction with cell parts.
         * Diffenrent cell perts are restricted to be in a specific state
         * (e.g. Walls can only be solids.)
         */
        enum State
        {
            SOLID,
            LIQUID,
            GAS
        };

    private:
        const State m_state;
        Qt3DRender::QMaterial* m_material;

    public:
        Material(State const state);
        ~Material();



};
}
