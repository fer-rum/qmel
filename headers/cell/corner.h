#pragma once

#include <Qt3DRender>
#include "headers/cell/part.h"

namespace qmel {
namespace cell {

class Corner :
        public Part
{
    public:
        enum Direction : int {
            NORTH_WEST = 0,
            NORTH_EAST = 1,
            SOUTH_EAST = 2,
            SOUTH_WEST = 3
        };

        // TODO implement later
        enum Style {
            SQUARE,
            OCAGONAL,
            ROUND
        };

    private:
        Direction m_direction;

    public:

        Corner(Direction direction, Qt3DRender::QMaterial* material, Cell* parent);
        ~Corner() = default;

    public slots:
        void updateGeometry();
};
} // namespace cell
} // namespace qmel
