import QtQuick 2.12
import Qt3D.Core 2.12
import Qt3D.Render 2.12
import Qt3D.Input 2.12
import Qt3D.Extras 2.12

Entity {
    id: sceneRoot
    objectName: "sceneRoot"

    components: [
        RenderSettings{
            activeFrameGraph: ForwardRenderer {
                clearColor: Qt.rgba(0, 0.5, 1, 1)
                camera: camera
            }
        },

        // Event Source will be set by the Qt3DQuickWindow
        InputSettings { }
    ]

    Camera {
        id: camera
        objectName: "camera"
        projectionType: CameraLens.PerspectiveProjection
        fieldOfView: 45
        aspectRatio: 16/9
        nearPlane : 0.25
        farPlane : 1000.0
        position: Qt.vector3d( 0.0, 1.0, 0.0 )
        upVector: Qt.vector3d( 0.0, 1.0, 0.0 )
        viewCenter: Qt.vector3d( 0.0, 0.0, 1.0 )

        onPositionChanged: camera_info_text.text =
                           "X: " + position.x + "\n" +
                           "Y: " + position.y + "\n" +
                           "Z: " + position.z

        // TODO replace this with something better suited
        OrbitCameraController {
            id: cameraController
            objectName: "cameraController"
            camera: camera
        }
    }

}
