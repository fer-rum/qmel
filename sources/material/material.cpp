#include "headers/material/material.h"

using namespace qmel;

Material::Material(State const state) :
    m_state{state},
    m_material{nullptr}
{}

Material::~Material()
{
    if(m_material != nullptr) { delete m_material; }
}
