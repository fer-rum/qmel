#pragma once

#include "headers/cell/part.h"

namespace qmel {
namespace cell {

/**
 * @brief The Horizontal class covers the case of cell floors and ceilings
 */
class Horizontal :
        public Part
{
    public:
        enum Direction : int
        {
            CEILING = 0, // +y direction
            FLOOR   = 1  // -y direction
        };

    private:
        Direction m_direction;

    public:

        Horizontal(Direction direction,
                   Qt3DRender::QMaterial* material,
                   Cell* parent);
        ~Horizontal() = default;

    public slots:
        void updateGeometry();

};

} // namespace cell
} // namespace qmel
