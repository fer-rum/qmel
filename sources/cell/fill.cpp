#include "headers/cell/fill.h"
#include "headers/cell/cell.h"

using namespace qmel::cell;

Fill::Fill(Qt3DRender::QMaterial* material,
           Cell* parent,
           unsigned char fillAmount)
    : Part(material, parent),
      m_fillAmount{fillAmount}
{
    updateGeometry();
}

void
Fill::updateGeometry(){
    // if a cell has no floor, any fill level > 0 will accomodate
    // for the floor thickness, only fill level == 0 will not
    // compensate the floor.
    // consecutively: fill level < max will not accomodate for the ceiling,
    // only fill level == max will

    float constexpr minOuter = 0.0f;
    float constexpr maxOuter = Cell::borderLength;
    float constexpr minInner = minOuter + Cell::wallThickness;
    float constexpr maxInner = maxOuter - Cell::wallThickness;

    float minX = cell()->hasWall(Wall::SOUTH)   ? minInner : minOuter;
    float maxX = cell()->hasWall(Wall::NORTH)   ? maxInner : maxOuter;
    float minZ = cell()->hasWall(Wall::WEST)    ? minInner : minOuter;
    float maxZ = cell()->hasWall(Wall::EAST)    ? maxInner : maxOuter;
    float minY = cell()->hasFloor()             ? minInner : minOuter;
    float maxY;

    if(m_fillAmount == minFillAmount) {
        maxY = minY;
    } else if(m_fillAmount == maxFillAmount) {
        maxY = cell()->hasCeiling() ? maxInner : maxOuter;
    } else {
        float constexpr deltaInner = maxInner - minInner;
        maxY = (deltaInner * m_fillAmount) / maxFillAmount + minInner;
    }

    Part::updateAABB({minX, minY, minZ}, {maxX, maxY, maxZ});
}

unsigned char
Fill::fillAmount() const
{
    return m_fillAmount;
}

void
Fill::setFillAmount(unsigned char newAmount)
{
    // check if the new amount is different first, to avoid unneccessary
    // geometry updates
    if(m_fillAmount != newAmount){
        m_fillAmount = newAmount;
        updateGeometry();
    }
}
