#include "headers/cell/horizontal.h"
#include "headers/cell/cell.h"

using namespace qmel::cell;

Horizontal::Horizontal(Direction direction,
                       Qt3DRender::QMaterial* material,
                       Cell* parent) :
    Part(material, parent),
    m_direction{direction}
{
    updateGeometry();
}

void
Horizontal::updateGeometry()
{
    QVector3D min = Cell::min;
    QVector3D max = Cell::max;

    float constexpr minInner = Cell::wallThickness;
    float constexpr maxInner = Cell::borderLength - Cell::wallThickness;

    switch(m_direction){
        case CEILING:
            min.setY(maxInner);
            break;
        case FLOOR:
            max.setY(minInner);
            break;
    }

    Part::updateAABB(min, max);
}
