﻿#include "headers/cell/cell.h"

using namespace qmel;
using namespace qmel::cell;

Cell::Cell(QEntity* parent) :
    Qt3DCore::QEntity (parent),
    m_horizontal{nullptr, nullptr},
    m_wall{nullptr, nullptr, nullptr, nullptr},
    m_corner{nullptr, nullptr, nullptr, nullptr},
    m_fill{nullptr}
{}

// === Cell::has...() ===
bool
Cell::hasFloor() const
{
    return m_horizontal[Horizontal::FLOOR] != nullptr;
}

bool
Cell::hasCeiling() const
{
    return m_horizontal[Horizontal::CEILING] != nullptr;
}

bool
Cell::hasWall(Wall::Direction where) const
{
    return m_wall[where] != nullptr;
}

bool
Cell::hasCorner(Corner::Direction where) const
{
    return m_corner[where] != nullptr;
}

bool
Cell::hasFill() const
{
    return m_fill != nullptr;
}

// === Cell::add...() ===

void
Cell::addHorizontal(Horizontal::Direction where,
                    Qt3DRender::QMaterial* material)
{
    Horizontal* newPart = new Horizontal(where, material, this);
    connect(this,       &Cell::signal_geometryChanged,
            newPart,    &Horizontal::updateGeometry);
    m_horizontal[where].reset(newPart);

    emit signal_geometryChanged();
}

void
Cell::addFloor(Qt3DRender::QMaterial* material)
{
    if(hasFloor()){
        qDebug() << "Attempt to create a floor where there already was one.";
        return;
    }

    addHorizontal(Horizontal::FLOOR, material);
}

void
Cell::addCeiling(Qt3DRender::QMaterial* material)
{
    if(hasCeiling()){
        qDebug() << "Attempt to create a ceiling where there already was one.";
        return;
    }

    addHorizontal(Horizontal::CEILING, material);
}

void
Cell::addCorner(Corner::Direction where, Qt3DRender::QMaterial* material)
{
    if(hasCorner(where)){
        qDebug() << "Attempt to create a corner where there already was one.";
        return;
    }

    Corner* newPart = new Corner(where, material, this);
    connect(this,       &Cell::signal_geometryChanged,
            newPart,    &Corner::updateGeometry);
    m_corner[where].reset(newPart);

    emit signal_geometryChanged();
}

void
Cell::addWall(Wall::Direction where, Qt3DRender::QMaterial* material)
{
    // TODO a wall can only be erected if the bounding corners exist!
    // auto-construct missing corners?
    if(hasWall(where)){
        qDebug() << "Attempt to create a wall where there already was one.";
        return;
    }

    Wall* newPart = new Wall(where, material, this);
    connect(this,       &Cell::signal_geometryChanged,
            newPart,    &Wall::updateGeometry);
    m_wall[where].reset(newPart);

    emit signal_geometryChanged();
}

void
Cell::addFill(Qt3DRender::QMaterial* material, unsigned char fillAmount)
{
    if(hasFill()){
        qDebug() << "Attempt to create a fill where there already was one.";
        return;
    }

    Fill* newPart = new Fill(material, this, fillAmount);
    connect(this,       &Cell::signal_geometryChanged,
            newPart,    &Fill::updateGeometry);
    m_fill.reset(newPart);

    emit signal_geometryChanged();
}

// === Cell::remove...() ===

void
Cell::removeHorizontal(Horizontal::Direction where)
{
    // Deleting a QObject subclass automatically disconnects
    m_horizontal[where].reset(nullptr);

    emit signal_geometryChanged();
}

void
Cell::removeFloor()
{
    removeHorizontal(Horizontal::FLOOR);
}

void
Cell::removeCeiling()
{
    removeHorizontal(Horizontal::CEILING);
}

void
Cell::removeWall(cell::Wall::Direction where)
{
    // Deleting a QObject subclass automatically disconnects
    m_wall[where].reset(nullptr);

    emit signal_geometryChanged();
}

void
Cell::removeCorner(cell::Corner::Direction where)
{
    // Deleting a QObject subclass automatically disconnects
    // TODO a corner should only be deleted if the adjacent walls are gone too.
    m_corner[where].reset(nullptr);

    emit signal_geometryChanged();
}

void
Cell::removeFill()
{
    m_fill.reset(nullptr);

    emit signal_geometryChanged();
}
