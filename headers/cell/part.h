#pragma once

#include <Qt3DCore>
#include <Qt3DRender>
#include <Qt3DExtras>

namespace qmel {

class Cell;

namespace cell {

class Part :
        public Qt3DCore::QEntity
{
    private:
        Cell* const m_cell; // caching the pointer to the parent as the correct type
        Qt3DRender::QMaterial* m_material;
        Qt3DExtras::QCuboidMesh* m_mesh; // TODO generalize into QMesh
        Qt3DCore::QTransform* m_transform;

    public:
        Part(/*QVector3D const& minPosition,
                              QVector3D const& maxPosition,*/
             Qt3DRender::QMaterial* material,
             Cell* parent);
        ~Part();

        void updateAABB(QVector3D const& minPosition,
                            QVector3D const& maxPosition);

        Cell* cell() const;

    public slots:
        virtual void updateGeometry() = 0;
};
}
}
