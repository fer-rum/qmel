#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QDebug>
#include <QQuickWindow>
#include <Qt3DCore>
#include <Qt3DRender>
#include <Qt3DExtras>

#include "headers/cell/cell.h"

int main(int argc, char *argv[])
{
    using namespace Qt3DCore;
    using namespace Qt3DRender;
    using namespace qmel;

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/mainWindow.qml")));

    if (engine.rootObjects().isEmpty()){
        // Failed to even launch the Window.
        return -1;
    }

    // Assume to have exactly the main window - get it
    Q_ASSERT(engine.rootObjects().count() == 1);
    QQuickWindow* mainWindow = qobject_cast<QQuickWindow*>(engine.rootObjects().first());
    Q_CHECK_PTR(mainWindow);

    QEntity* sceneRoot = mainWindow->findChild<QEntity*>("sceneRoot");
    Q_CHECK_PTR(sceneRoot);

    QCamera* camera = sceneRoot->findChild<QCamera*>("camera");
    Q_CHECK_PTR(camera);

    // create something to show

    Qt3DExtras::QGoochMaterial* material_gooch = new Qt3DExtras::QGoochMaterial(sceneRoot);
    material_gooch->setCool(QColor(127, 127, 255));
    material_gooch->setWarm(QColor(255, 127, 127));
    material_gooch->setDiffuse(QColor(127, 127, 127));
    material_gooch->setSpecular(QColor(0, 255, 0));


    Qt3DExtras::QMorphPhongMaterial* material_plain = new Qt3DExtras::QMorphPhongMaterial (sceneRoot);
    material_plain->setAmbient(QColor(127, 127, 127));
    material_plain->setDiffuse(QColor(255, 127, 0));

    Qt3DExtras::QMorphPhongMaterial* material_stone = new Qt3DExtras::QMorphPhongMaterial (sceneRoot);
    material_stone->setAmbient(QColor(127, 127, 127));
    material_stone->setDiffuse(QColor(127, 127, 127));

    Cell* myCell = new Cell(sceneRoot);
    Q_CHECK_PTR(myCell);

    myCell->addFloor(material_plain);
    myCell->addWall(cell::Wall::NORTH, material_stone);
    myCell->addWall(cell::Wall::EAST, material_stone);
    myCell->addFill(material_gooch, 64);

    QPointLight* light = new QPointLight();
    sceneRoot->addComponent(light);

    return app.exec();
}
