
#include "headers/cell/cell.h"
#include "headers/cell/part.h"

using namespace qmel::cell;

Part::Part(/*QVector3D const& minPosition,
                   QVector3D const& maxPosition,*/
                   Qt3DRender::QMaterial* material,
                   Cell* parent) :
    Qt3DCore::QEntity (parent),
    m_cell{parent},
    m_material{material},
    m_mesh{new Qt3DExtras::QCuboidMesh},
    m_transform{new Qt3DCore::QTransform}
{
    Q_CHECK_PTR(parent);

    //this->updateGeometry(minPosition, maxPosition);
    this->addComponent(m_mesh);
    this->addComponent(m_transform);

    // assign the material
    Q_CHECK_PTR(material);
    this->addComponent(material);
}

Part::~Part()
{
    if( m_material != nullptr ){
        // NOTE: Calling remove here for some reason segfaults
        // maybe the material gets deleted before?
        // this->removeComponent(m_material);
        // DO NOT DELETE MATERIAL. You do not own this!
        // It belongs to whoever gave it to you.
    }

    // DO delete the mesh, it is your responsibility
    this->removeComponent(m_mesh);
    delete m_mesh;

    // DO delete the transform, it is your responsibility
    this->removeComponent(m_transform);
    delete m_transform;
}

void
Part::updateAABB(QVector3D const& minPosition,
                         QVector3D const & maxPosition)
{
    Q_CHECK_PTR(m_mesh);
    Q_CHECK_PTR(m_transform);

    // Validate input
    // Assuming that none of dimensions are 0, paper wall is not supported
    // Check that min and max are not switched
    Q_ASSERT(minPosition.x() < maxPosition.x());
    Q_ASSERT(minPosition.y() < maxPosition.y());
    Q_ASSERT(minPosition.z() < maxPosition.z());

    // Update the geometry
    m_mesh->setXExtent(maxPosition.x() - minPosition.x());
    m_mesh->setYExtent(maxPosition.y() - minPosition.y());
    m_mesh->setZExtent(maxPosition.z() - minPosition.z());

    // apply the transformation
    // TODO the anchor of a cuboid is its center! Adjust code more proper
    // by passing extend vector only (center is implicit from cell constants and direction?)
    m_transform->setTranslation((minPosition + maxPosition) / 2);
}

qmel::Cell*
Part::cell() const{
    return m_cell;
}
