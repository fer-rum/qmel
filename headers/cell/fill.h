#pragma once

#include "headers/cell/part.h"

namespace qmel {
namespace cell {

class Fill :
        public Part
{


    private:
        unsigned char m_fillAmount;


    public:
        unsigned char minFillAmount = 0x00;
        unsigned char maxFillAmount = 0xFF;

        Fill(Qt3DRender::QMaterial* material,
             Cell* parent,
             unsigned char fillAmount = 0);
        ~Fill() = default;

        unsigned char fillAmount() const;

        /**
         * @brief setFillAmount
         * @param newAmount
         * Changing the fill amount will trigger a geometry update.
         */
        void setFillAmount(unsigned char newAmount);

    public slots:
        void updateGeometry();

};

} // namespace cell
} // namespace qmel
