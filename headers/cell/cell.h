#pragma once

#include <memory>
#include <Qt3DCore>
#include <Qt3DRender>
#include <Qt3DExtras>
#include "headers/cell/part.h"
#include "headers/cell/wall.h"
#include "headers/cell/corner.h"
#include "headers/cell/fill.h"
#include "headers/cell/horizontal.h"

namespace qmel {

/**
 * @brief The Cell class abstracts a cubic space in the world geometry.
 * A cell is made up of different parts:
 *  * Horizontals for floor and ceiling
 *  * Walls and corners for the vertical boundaries
 *  * Fills for the center space
 *  Each of these components is optional.
 *
 * While floor and ceiling are conceptually the same thing, they are exposed as
 * two separate things since it feels more natural to talk about them seperately
 * than via a made-up generic term.
 */
class Cell : // TODO subdivide into different cell types?
        public Qt3DCore::QEntity
{
        Q_OBJECT

    private:

        // === Cell borders ===
        // TODO switch to unique pointers
        std::unique_ptr<cell::Horizontal> m_horizontal[2];  // c.f. cell::Horizontal::Direction
        std::unique_ptr<cell::Wall> m_wall[4];              // c.f. cell::Wall::Direction
        std::unique_ptr<cell::Corner> m_corner[4];          // c.f. cell::Corner::Direction
        std::unique_ptr<cell::Fill> m_fill;

        void addHorizontal(cell::Horizontal::Direction where,
                           Qt3DRender::QMaterial* material);

        void removeHorizontal(cell::Horizontal::Direction where);

    signals:
        /**
         * @brief recalculateMeshes re-adjusts the mesh children
         * depending on the set materials.
         * Call this when changing the borders or the fill.
         */
        void signal_geometryChanged();

    public:
        static constexpr float borderLength = 2.0f;
        static constexpr float wallThickness = 0.25f;
        static constexpr QVector3D min {0.0f, 0.0f, 0.0f};
        static constexpr QVector3D max {borderLength, borderLength, borderLength};

        Cell(QEntity* parent);
        ~Cell() = default;

        bool hasFloor() const;
        bool hasCeiling() const;
        bool hasWall(cell::Wall::Direction where) const;
        bool hasCorner(cell::Corner::Direction where) const;
        bool hasFill() const;

        void addFloor(Qt3DRender::QMaterial* material);

        void addCeiling(Qt3DRender::QMaterial* material);

        void addWall(cell::Wall::Direction where,
                     Qt3DRender::QMaterial* material);

        void addCorner(cell::Corner::Direction where,
                       Qt3DRender::QMaterial* material);

        void addFill(Qt3DRender::QMaterial* material,
                     unsigned char fillAmount);

        void removeFloor();
        void removeCeiling();
        void removeWall(cell::Wall::Direction where);
        void removeCorner(cell::Corner::Direction where);
        void removeFill();
};
}
