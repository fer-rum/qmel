#pragma once

#include "headers/cell/part.h"

namespace qmel {
namespace cell {

class Wall :
        public Part
{
    public:
        enum Direction : int
        {
            NORTH   = 0, // +x direction
            SOUTH   = 1, // -x direction
            EAST    = 2, // +z direction
            WEST    = 3  // -z direction
        };

    private:
        Direction m_direction;
        // TODO doors and portals

    public:

        Wall(Wall::Direction direction,
             Qt3DRender::QMaterial* material,
             Cell* parent);
        ~Wall() = default;

    public slots:
        void updateGeometry();
};
} // namespace cell
} // namespace qmel
