import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Scene3D 2.12

Window {
    id: mainWindow
    visible: true
    width: 640
    height: 480
    title: qsTr("Qmel")

    Scene3D {
        objectName: "scene3d"
        id: scene3d
        anchors.fill: parent
        anchors.margins: 10
        focus: true
        aspects: ["input", "logic"]
        cameraAspectRatioMode: Scene3D.AutomaticAspectRatio

        entity:SceneRoot{}
    }

    Rectangle{
        id: camera_info
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.top: parent.top
        anchors.topMargin: 10
        anchors.margins: 10
        color: "#808080"
        width: camera_info_text.width + 4
        height: camera_info_text.height + 4

        Text {
            id: camera_info_text
            x: 2
            y: 2
            text: "No Data"
        }
    }
}
