#include "headers/cell/wall.h"
#include "headers/cell/cell.h"

using namespace qmel::cell;

Wall::Wall(Wall::Direction direction,
           Qt3DRender::QMaterial* material,
           Cell* parent) :
    Part(material, parent),
    m_direction{direction}
{
    updateGeometry();
}

void
Wall::updateGeometry()
{
    // TODO: how to deal with different corner sizes / shapes?

    float constexpr minOuter = 0.0f;
    float constexpr maxOuter = Cell::borderLength;
    float constexpr minInner = minOuter + Cell::wallThickness;
    float constexpr maxInner = maxOuter - Cell::wallThickness;

    float minY = cell()->hasFloor()? minInner : minOuter;
    float maxY = cell()->hasCeiling()? maxInner : maxOuter;


    switch(m_direction)
    {
        case Direction::NORTH:
            Part::updateAABB(
                {maxInner, minY, minInner},
                {maxOuter, maxY, maxInner} );
            break;
        case Direction::SOUTH:
            Part::updateAABB(
                {minOuter, minY, minInner},
                {minInner, maxY, maxInner} );
            break;
        case Direction::EAST:
            Part::updateAABB(
                {minInner, minY, maxInner},
                {maxInner, maxY, maxOuter} );
            break;
        case Direction::WEST:
            Part::updateAABB(
                {minInner, minY, minOuter},
                {maxInner, maxY, minInner} );
            break;
    }
}
