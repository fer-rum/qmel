#include "headers/cell/corner.h"
#include "headers/cell/cell.h"

using namespace qmel::cell;

Corner::Corner(Direction direction,
               Qt3DRender::QMaterial* material,
               Cell* parent) :
    Part(material, parent),
    m_direction{direction}
{
    updateGeometry();
}

void
Corner::updateGeometry()
{
    // TODO: how to deal with different corner sizes / shapes?

    float constexpr minOuter = 0.0f;
    float constexpr maxOuter = Cell::borderLength;
    float constexpr minInner = minOuter + Cell::wallThickness;
    float constexpr maxInner = maxOuter - Cell::wallThickness;

    float minY = cell()->hasFloor()? minInner : minOuter;
    float maxY = cell()->hasCeiling()? maxInner : maxOuter;


    switch(m_direction)
    {
        case Direction::NORTH_WEST:
            Part::updateAABB(
                {maxInner, minY, minOuter},
                {maxOuter, maxY, minInner} );
            break;
        case Direction::NORTH_EAST:
            Part::updateAABB(
                {maxInner, minY, maxInner},
                {maxOuter, maxY, maxOuter} );
            break;
        case Direction::SOUTH_EAST:
            Part::updateAABB(
                {minOuter, minY, maxInner},
                {minInner, maxY, maxOuter} );
            break;
        case Direction::SOUTH_WEST:
            Part::updateAABB(
                {minOuter, minY, minOuter},
                {minInner, maxY, minInner} );
            break;
    }
}
